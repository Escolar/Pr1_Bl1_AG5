cmake_minimum_required(VERSION 3.12)
project(Pr1_Bl1_AG5 C)

set(CMAKE_C_STANDARD 99)

add_executable(Pr1_Bl1_AG5 main.c)