// Author: Malte Schöppe
// This program can be used to change between currencies

#include <stdio.h>

int main() {
    const float rateEurCzk = 25.8407;
    const float rateCzkEur = 1 / rateEurCzk;

    printf("EUR \t CZK\n");
    for(int i = 0; i <= 100; i++) {
        if(!(i == 1 || i == 2 || i == 5 || i == 10 || i == 20 || i == 50 || i == 100)) continue;

        printf("%i \t %.2f \n", i, i * rateEurCzk);
    }

    printf("\n");

    printf("CZK \t EUR\n");
    for(int i = 0; i <= 100; i++) {
        if(!(i == 1 || i == 2 || i == 5 || i == 10 || i == 20 || i == 50 || i == 100)) continue;

        printf("%i \t %.2f \n", i, i * rateCzkEur);
    }

    return 0;
}